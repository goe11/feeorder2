import React ,{ Component } from 'react';
import * as iconsAi         from 'react-icons/ai';
import * as iconsVsc        from 'react-icons/vsc';
import * as iconsCg         from "react-icons/cg"; 
import * as iconsBs         from 'react-icons/bs';
import * as iconsFi         from 'react-icons/fi';
import imgBayuBuana         from '../image/bayu_buana.png';
import imgAvatar            from '../image/avatar.png';
import Dropdown             from 'react-bootstrap/Dropdown';

export class Navigation extends Component {
    constructor(props) {
        super(props);
        this.dropDownProfile = this.dropDownProfile.bind(this)
        this.state = {
            checkNotif  : true,
            ddProfile   : false, 
        }
    }

    dropDownProfile(){
        this.setState({
            ddProfile: !this.state.ddProfile
        })
    }

    render(){
        return(
            <div className='container'>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid">
                        <img src={imgBayuBuana} width={100} style={{ cursor:"pointer" }} />
                        <form class="d-flex">
                            <iconsAi.AiFillQuestionCircle className='mt-1 me-3' size={30} style={{ cursor:"pointer" }} />
                            {
                                this.state.checkNotif ?
                                <iconsAi.AiOutlineBell className='mt-1 me-3' size={30} style={{ cursor:"pointer" }} />
                                :
                                <iconsVsc.VscBellDot className='mt-1 me-3' size={30} style={{ cursor:"pointer" }} color="red" />
                            }
                            <Dropdown>
                                <Dropdown.Toggle variant="white" id="dropdown-basic">
                                    <img src={imgAvatar} className="mt-n3" style={{ width:"35px", borderRadius:"50%", cursor:"pointer" }} />
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <div className='col-12'>
                                        <div className='row'>
                                            <div className='col-4'>
                                                <img src={imgAvatar} className="ms-2" style={{ width:"35px", borderRadius:"50%", cursor:"pointer" }} />
                                            </div>
                                            <div className='col-8 mt-n3'>
                                                <p className='fw-bold' style={{ fontSize:'0.9rem' }}>Patrick Jane</p>
                                                <p className='lead'  style={{ fontSize:'0.9rem' }}>Administrator</p>
                                            </div>
                                        </div>
                                        <div className='row mt-n3'>
                                            <div className='col-12'>
                                                <hr></hr>
                                            </div>
                                        </div>
                                    </div>
                                    <Dropdown.Item className="pb-3" href="#/action-1"><iconsCg.CgProfile size={20} /> My Profile</Dropdown.Item>
                                    <Dropdown.Item className="pb-3" href="#/action-2"><iconsBs.BsFillUnlockFill size={20} />Change Password</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3"><iconsFi.FiLogOut size={20} />Sign Out</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </form>
                    </div>
                </nav>
            </div>
        )
    }
}

export default Navigation