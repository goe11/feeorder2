import React ,{ Component } from 'react';
import * as iconsFa from 'react-icons/fa';
import * as iconsAi from 'react-icons/ai';
import * as iconsRi from 'react-icons/ri';
import {DropdownButton, Dropdown} from 'react-bootstrap';
import imgUpDown    from '../../image/updown.png';
import Pagination from 'react-bootstrap/Pagination';


export class markup extends Component {
    constructor(props) {
        super(props);
        this.changeTable    = this.changeTable.bind(this)
        this.state = {
            clickTabTable:0
        }
    }

    changeTable(id){
        this.setState({
            clickTabTable: id
        })
    }

   

    render(){
        let active = 1;
        let items = [];
        for (let number = 1; number <= 5; number++) {
            items.push(
                <Pagination.Item key={number} active={number === active}>
                {number}
                </Pagination.Item>,
            );
        }
        const paginationBasic = (
            <div>
              <Pagination style={{ color:'black' }} >{items}</Pagination>
            </div>
        );
        return(
            <div className='row'>
                <div className='col-12' style={{ backgroundColor:'whitesmoke' }}>
                    <div className='row'>
                        <div className='col-2' onClick={() => this.changeTable(0)} style={{ cursor:'pointer' ,backgroundColor: this.state.clickTabTable == 0 ? 'white':null, boxShadow:this.state.clickTabTable == 0 ? '2px 2px 4px #000000':null  }} >
                            {
                                this.state.clickTabTable == 0 ?
                                    <div style={{ borderTop:'6px solid yellow' }} />
                                :null
                            }
                            <iconsRi.RiFlightTakeoffFill size={30} /> <span>FLIGHT</span>
                        </div>
                        <div className='col-2' onClick={() => this.changeTable(1)} style={{ cursor:'pointer' ,backgroundColor: this.state.clickTabTable == 1 ? 'white':null, boxShadow:this.state.clickTabTable == 1 ? '2px 2px 4px #000000':null  }}>
                            {
                                this.state.clickTabTable == 1 ?
                                    <div style={{ borderTop:'6px solid yellow' }} />
                                :null
                            }
                            <iconsFa.FaHotel size={30}  />  <span>HOTEL</span>
                        </div>
                        <div className='col-2' onClick={() => this.changeTable(2)} style={{ cursor:'pointer' ,backgroundColor: this.state.clickTabTable == 2 ? 'white':null, boxShadow:this.state.clickTabTable == 2 ? '2px 2px 4px #000000':null  }}>
                            {
                                this.state.clickTabTable == 2 ?
                                    <div style={{ borderTop:'6px solid yellow' }} />
                                :null
                            }
                            <iconsRi.RiMessage3Fill size={30}  /> <span>OTHER</span>
                        </div>
                    </div>
                    <div className='row mt-3'>
                        <div className='col-3'>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Search..." aria-label="Recipient's username" aria-describedby="basic-addon2" />
                                <span class="input-group-text" id="basic-addon2" style={{ backgroundColor:'white' }}><iconsAi.AiOutlineSearch /></span>
                            </div>
                        </div>
                    </div>
                    <table class="table" style={{color:'black', backgroundColor:'whitesmoke'}}>
                        {
                            this.state.clickTabTable == 2 ?
                                <thead>
                                    <tr>
                                        <th scope="col">Preset Name <span><img src={imgUpDown} width={50} style={{ cursor:"pointer" }} /></span> </th>
                                        <th>Actions</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Other Mark-Up 1</th>
                                        <td><iconsRi.RiEditBoxFill size={30} color="blue" /> <span><iconsRi.RiDeleteBin5Line color="blue" size={30} /></span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Other Mark-Up 2</th>
                                        <td><iconsRi.RiEditBoxFill size={30} color="blue" /> <span><iconsRi.RiDeleteBin5Line color="blue" size={30} /></span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Other Mark-Up 3</th>
                                        <td><iconsRi.RiEditBoxFill size={30} color="blue" /> <span><iconsRi.RiDeleteBin5Line color="blue" size={30} /></span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Other Mark-Up 4</th>
                                        <td><iconsRi.RiEditBoxFill size={30} color="blue" /> <span><iconsRi.RiDeleteBin5Line color="blue" size={30} /></span></td>
                                    </tr>
                                </thead>
                            :
                            <>
                                <thead>
                                    <tr>
                                    {/* <HiOutlineChevronUpDown size={20} /> */}
                                        <th scope="col">Preset Name <span><img src={imgUpDown} width={50} style={{ cursor:"pointer" }} /></span> </th>
                                        <th scope="col">Domestic Mark Up <span><iconsRi.RiArrowUpSLine size={30} /></span> </th>
                                        <th scope="col">International Mark Up <span><iconsRi.RiArrowUpSLine size={30} /></span></th>
                                        <th scope="col">Number of Override <span><iconsRi.RiArrowUpSLine size={30} /></span></th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>{this.state.clickTabTable == 0 ? "Flight" : "Hotel"} Mark-up 1</th>
                                        <td>IDR {this.state.clickTabTable == 0 ? "9,5000/Ticket": "75,000/Room Night"}</td>
                                        <td>IDR {this.state.clickTabTable == 0 ? "110,000/Ticket": "100,000/Room Night"}</td>
                                        <td>3</td>
                                        <td><iconsRi.RiEditBoxFill size={30} color="blue" /> <span><iconsRi.RiDeleteBin5Line color="blue" size={30} /></span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">{this.state.clickTabTable == 0 ? "Flight" : "Hotel"} Mark-up 2</th>
                                        <td>5% Include Tax</td>
                                        <td>5% Exclude Tax</td>
                                        <td>1</td>
                                        <td><iconsRi.RiEditBoxFill size={30} color="blue" /> <span><iconsRi.RiDeleteBin5Line color="blue" size={30} /></span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">{this.state.clickTabTable == 0 ? "Flight" : "Hotel"} Mark-up 3</th>
                                        <td>IDR {this.state.clickTabTable == 0 ? "125,000/Ticket": "200,000/Transaction"}</td>
                                        <td>IDR {this.state.clickTabTable == 0 ? "200,000/Ticket": "325,000/Transaction"}</td>
                                        <td>2</td>
                                        <td><iconsRi.RiEditBoxFill size={30} color="blue" /> <span><iconsRi.RiDeleteBin5Line color="blue" size={30} /></span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">{this.state.clickTabTable == 0 ? "Flight" : "Hotel"} Mark-up 4</th>
                                        <td>3% Include Tax</td>
                                        <td>3% Exclude Tax</td>
                                        <td>3</td>
                                        <td><iconsRi.RiEditBoxFill size={30} color="blue" /> <span><iconsRi.RiDeleteBin5Line color="blue" size={30} /></span></td>
                                    </tr>
                                </tbody>
                            </>
                        }
                    </table>
                    <div className='row'>
                        <div className='col-1'>
                            <DropdownButton variant="light" id="dropdown-basic-button" title="4">
                                <Dropdown.Item href="#/action-1">1</Dropdown.Item>
                                <Dropdown.Item href="#/action-2">2</Dropdown.Item>
                                <Dropdown.Item href="#/action-3">3</Dropdown.Item>
                                <Dropdown.Item href="#/action-3">4</Dropdown.Item>
                            </DropdownButton>
                        </div>
                        <div className='col-2 mt-2'>
                            Showing 1 - 4 of 4
                        </div>
                        <div className='col-5'></div>
                        <div className='col-1'>Page:</div>
                        <div className='col-3'><span>{paginationBasic}</span></div>
                    </div>
                </div>
                
            </div>
        )
    }
}

export default markup