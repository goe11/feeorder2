import React ,{ Component } from 'react';
import * as iconsAi from 'react-icons/ai';
import Markup from './table/markup';
import MarkUpHotel from './table/markuphotel';

export class Body extends Component {
    constructor(props) {
        super(props);
        this.mouseHovers        = this.mouseHovers.bind(this)
        this.mouseHoversOut     = this.mouseHoversOut.bind(this)

        this.state = {
            management: "Standard Mark Up",
        }
    }

    mouseHovers(event){
        event.target.style.color = 'red';
    }

    mouseHoversOut(event){
        event.target.style.color = 'black';
    }

    render(){
        return(
            <div className='container'>
                <div className='row mt-3'>
                    <div className='col-12'>
                        <p className='lead' >Master Data Management <iconsAi.AiOutlineRight /> <span onMouseEnter={(event) => this.mouseHovers(event)} onMouseLeave={(event) => this.mouseHoversOut(event)} >{this.props.tempTitle}</span></p>
                    </div>
                </div>
                <div className='row mt-3'>
                    <div className='col-12'>
                        <p className='fs-3 fw-bold lead'>{this.props.tempTitle}</p>
                    </div>
                </div>
                <div className='row mt-3'>
                    <div className='col-12'>
                    {
                        this.props.tempId == 0 ?
                            <Markup />
                        :this.props.tempId == 1 ?
                            <MarkUpHotel />
                        :null
                    }
                    </div>
                </div>
            </div>
        )
    }
}

export default Body