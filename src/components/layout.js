import React ,{ Component } from 'react';
import Navigation   from '../components/navbar';
import { Body }     from '../components/body';
import * as iconsAi from 'react-icons/ai';
import * as iconsBs from 'react-icons/bs'

export class Layout extends Component {
    constructor(props) {
        super(props);
        this.navSide        = this.navSide.bind(this)
        this.navSideIn      = this.navSideIn.bind(this)
        this.database       = this.database.bind(this)
        this.clickSideNav   = this.clickSideNav.bind(this)
        this.state = {
            navSideVal:'63px',
            flagSideNav: false,
            arrSideNav:[
                {id:0,title:"Standard Mark-Up"},
                {id:1,title:"Standart Service Fee"},
                {id:2, title:'Fee Type'},
                {id:3, title:'Frequent Traveler Program'},
                {id:4, title:'Standart Ancillary Fee'},
                {id:5, title:'Rating Type'},
                {id:6, title:'Setup Flight Commision'},
                {id:7, title:'Special Dates'},
                {id:8, title:'Corporate Rating'},
            ],
            colorSelected: 0,
            tempTitleSelected: 'Standard Mark-Up'
        }
        this.database()
    }

    navSide(event){
        this.setState({
            navSideVal:'280px',
            flagSideNav:true
        })
    }

    navSideIn(event){
        this.setState({
            navSideVal:'63px',
            flagSideNav:false
        })
    }

    clickSideNav(id, title){
        this.setState({
            colorSelected: id,
            tempTitleSelected: title
        })
    }

    database(){
        // let table = new DataTable('#example');
        // console.log(table)
    }


    render(){
        return(
            <div className='container'>
                <div className='row'>
                    <div className='col-1' onMouseEnter={(event) => this.navSide(event)} onMouseLeave={(event) => this.navSideIn(event)} >
                        <div className='position-absolute p-1' style={{ backgroundColor:'green', cursor:'pointer',zIndex:"30", width:this.state.navSideVal }} >
                            <iconsAi.AiOutlineHome className='mt-3 ms-2 mb-3' size={30} color="white" />{this.state.flagSideNav?<span className='fw-bold ms-2' style={{ color:'white' }}>Dashboard</span>:""}
                            <div className='col-12' style={{ backgroundColor:'orange' }} >
                                <iconsBs.BsFillBagFill className='mt-1 ms-2 mb-1' size={30} color="white" />{this.state.flagSideNav?<span className='fw-bold ms-2' style={{ color:'white' }}>Master Data Management<iconsAi.AiOutlineRight /></span>:""}
                            </div>
                            {
                                this.state.flagSideNav? 
                                    <div className='col-12'>
                                        <div className='row'>
                                            <div className='col-2'></div>
                                            <div className='col-10 fw-bold' style={{ color:'white' }} >
                                            {
                                                this.state.arrSideNav.map((data, index) => {
                                                    return(
                                                        <p key={index} onClick={() => this.clickSideNav(data.id, data.title)} style={{ color:this.state.colorSelected == data.id ? "yellow": "white" }} >{data.title}</p>
                                                    )
                                                })
                                            } 
                                            </div>
                                        </div>
                                    </div>
                                :null
                            }
                            
                        </div>
                    </div>
                    <div className='col-11'>
                        <Navigation style={{ zIndex:"-1" }} ></Navigation>
                        <Body tempTitle={this.state.tempTitleSelected} tempId={this.state.colorSelected} ></Body>
                    </div>
                </div>
            </div>
        )
    }
}

export default Layout