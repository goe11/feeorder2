import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Layout from './components/layout';

function App() {
  return (
    <Layout />
  );
}

export default App;
